package com.SQL;

public class Main {

    public static void main(String[] args) {

        JdbcAccess.dBConnection();

        System.out.println("Total number of citizens is " + SqlCommands.countOfCitizens() + "\n");
        System.out.println("Average age of citizens is " + SqlCommands.averageAgeOfCitizens() + "\n");
        System.out.println("Sorted alphabetically list of last names without repetitions:\n"
                + SqlCommands.ascendingListOfCitizensByLastName() + "\n");
        System.out.println("Sorted list of last names, indicating the number of their repetitions:\n"
                + SqlCommands.ascendingListOfCitizensByLastNameWithCount() + "\n");
        System.out.println("List of last names that contain the letter \"б\":\n"
                + SqlCommands.listOfCitizensContainBInLastName() + "\n");
        System.out.println("List of \"homeless people\":\n" + SqlCommands.listOfCitizensWithoutHome() + "\n");
        System.out.println("List of minors living on Pravdy Avenue:\n"
                + SqlCommands.listOfMinorsLivingOnSpecificStreet() + "\n");
        System.out.println("List of all streets in an alphabetical with how many residents live on the street:\n"
                + SqlCommands.listOfStreetsWithNumberCitizensCount() + "\n");
        System.out.println("List of streets, the name of which consists of 6 letters:\n"
                + SqlCommands.listOfStreetsWithSixLettersNumber() + "\n");
        System.out.println("List of streets with less than 3 citizens:\n"
                + SqlCommands.listOfStreetsWithSpecificCitizenCount() + "\n");

        JdbcAccess.dbDisconnect();
    }
}
