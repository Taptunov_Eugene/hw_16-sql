package com.SQL;

import java.sql.*;

public class JdbcAccess {

    static final String URL = "jdbc:ucanaccess://C://Users//Jack//IdeaProjects//SQL//src//main//resources//Database21.accdb";

    private static Connection connection;
    private static Statement statement;

    public static void dBConnection() {

        try {
            connection = DriverManager.getConnection(URL);
            statement = connection.createStatement();
        } catch (SQLException sqlEx) {
            System.out.println("Connection Failed!");
            sqlEx.printStackTrace();
        }
        if (connection != null) {
            System.out.println("Connection successful!");
        } else {
            System.out.println("Failed to connect!");
        }
    }

    public static void dbDisconnect() {
        try {
            if (connection != null && !connection.isClosed()) {
                connection.close();
            }
            System.out.println("Connection was closed!");
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    public static Statement getStatement() {
        return statement;
    }

    public static void setStatement(Statement statement) {
        JdbcAccess.statement = statement;
    }

}
