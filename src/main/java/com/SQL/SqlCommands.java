package com.SQL;

import org.w3c.dom.stylesheets.LinkStyle;

import java.util.HashMap;
import java.util.List;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;

public class SqlCommands {

    private static ResultSet resultSet;

    public static int countOfCitizens() {

        String query = "SELECT COUNT(*) FROM Person";
        int count = 0;
        try {
            resultSet = JdbcAccess.getStatement().executeQuery(query);
            while (resultSet.next()) {
                count = resultSet.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return count;
    }

    public static int averageAgeOfCitizens() {

        String query = "SELECT AVG(age) FROM Person";
        int average = 0;
        try {
            resultSet = JdbcAccess.getStatement().executeQuery(query);
            while (resultSet.next()) {
                average = resultSet.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return average;
    }

    public static List<String> ascendingListOfCitizensByLastName() {

        String query = "SELECT DISTINCT lastName FROM Person ORDER BY lastName ASC";
        List<String> arrayList = new ArrayList<>();
        try {
            resultSet = JdbcAccess.getStatement().executeQuery(query);
            while (resultSet.next()) {
                arrayList.add(resultSet.getString(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return arrayList;
    }

    public static Map<String, String> ascendingListOfCitizensByLastNameWithCount() {

        String query = "SELECT lastName, COUNT(*) FROM Person GROUP BY lastName";
        Map<String, String> map = new HashMap<>();
        try {
            resultSet = JdbcAccess.getStatement().executeQuery(query);
            while (resultSet.next()) {
                map.put(resultSet.getString(1), resultSet.getString(2));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return map;
    }

    public static List<String> listOfCitizensContainBInLastName() {

        String query = "SELECT lastName FROM Person WHERE lastName LIKE '%б%_'";
        List<String> arrayList = new ArrayList<>();
        try {
            resultSet = JdbcAccess.getStatement().executeQuery(query);
            while (resultSet.next()) {
                arrayList.add(resultSet.getString(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return arrayList;
    }

    public static ArrayList<String> listOfCitizensWithoutHome()  {
        String query = "SELECT * FROM Person WHERE idStreet IS NULL";
        ArrayList<String> arrayList = new ArrayList<>();
        try {
            resultSet = JdbcAccess.getStatement().executeQuery(query);
            while (resultSet.next()) {
                String persons = "";
                persons = "\nid=" + resultSet.getString(1) + ", firstName=" + resultSet.getString(2)
                        + ", lastName=" + resultSet.getString(3) + ", age=" + resultSet.getString(4);
                arrayList.add(persons);
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return arrayList;
    }

    public static List<String> listOfMinorsLivingOnSpecificStreet()  {
        String query = "SELECT * FROM Person JOIN street ON Person.idStreet = Street.id " +
                "WHERE UPPER(name) LIKE '%ПРОСПЕКТ ПРАВДЫ%' AND Person.age < 18";
        List<String> arrayList = new ArrayList<>();
        try {
            resultSet = JdbcAccess.getStatement().executeQuery(query);
            while (resultSet.next()) {
                String persons = "";
                persons = "\nid=" + resultSet.getString(1) + ", firstName=" + resultSet.getString(2)
                        + ", lastName=" + resultSet.getString(3) + ", age=" + resultSet.getString(4)
                        + ", idStreet=" + resultSet.getString(5);
                arrayList.add(persons);
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return arrayList;
    }

    public static Map<String, String> listOfStreetsWithNumberCitizensCount()  {
        String query = "SELECT name, COUNT(id) FROM Street JOIN Person ON Street.id = Person.idStreet " +
                "GROUP BY name";
        Map<String, String> map = new HashMap<>();
        try {
            resultSet = JdbcAccess.getStatement().executeQuery(query);
            while (resultSet.next()) {
                map.put(resultSet.getString(1), resultSet.getString(2));
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return map;
    }

    public static List listOfStreetsWithSixLettersNumber()  {
        String query = "SELECT * FROM Street WHERE LENGTH(name) = 6";
        List<String> arrayList = new ArrayList<>();
        try {
            resultSet = JdbcAccess.getStatement().executeQuery(query);
            while (resultSet.next()) {
                arrayList.add(resultSet.getString(1));
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return arrayList;
    }

    public static List<String> listOfStreetsWithSpecificCitizenCount(){
        String query = "SELECT name FROM Street JOIN Person ON Street.id = Person.idStreet" +
                " GROUP BY name" +
                " HAVING COUNT(Person.id) < 3";
        List<String> arrayList = new ArrayList<>();
        try {
            resultSet = JdbcAccess.getStatement().executeQuery(query);
            while (resultSet.next()) {
                arrayList.add(resultSet.getString(1));
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return arrayList;
    }

}
